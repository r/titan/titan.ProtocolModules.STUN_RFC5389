///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2000-2019 Ericsson Telecom AB
//
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v2.0
// which accompanies this distribution, and is available at
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
///////////////////////////////////////////////////////////////////////////////
//
//  File:               STUN_Functions.ttcn
//  Description:        
//  Rev:                R2B
//  Prodnr:             CNL 113 778
//  Updated:            2013-05-27
//  Contact:            http://ttcn.ericsson.se
//  Reference:          
//
//
module STUN_Functions {

import from STUN_Types all;
import from General_Types all;
  

function f_STUN_Create_Message_Header(
  inout PDU_STUN pl_pdu,
  in STUN_Message_Type pl_message_type,
  in integer pl_message_length,
  in OCT12 pl_transaction_id := '000000000000000000000000'O) {
    
  if (match(pl_transaction_id, '000000000000000000000000'O)) {
    pl_transaction_id := ef_STUN_Generate_Transaction_Id();
  }
  
  pl_pdu := {
    STUN_message_header := {
      message_type := pl_message_type,
      message_length := pl_message_length,
      magic_cookie := cg_magic_cookie,
      transaction_id := pl_transaction_id
    },
    STUN_attributes := {}
  }
  
} //f_STUN_Create_Message_Header

function f_STUN_Append_Mapped_Address(
  inout PDU_STUN pl_pdu,
  in STUN_Address pl_address
){
  pl_pdu.STUN_attributes[sizeof(pl_pdu.STUN_attributes)] := {
    attribute_type := MAPPED_ADDRESS,
    attribute_length := f_get_Address_Length(pl_address),
    attribute_value := {
      mapped_address := pl_address 
    }
  }
  
} //f_STUN_Append_Mapped_Address

function f_STUN_Append_Username(
  inout PDU_STUN pl_pdu,
  in charstring pl_username
){
  pl_pdu.STUN_attributes[sizeof(pl_pdu.STUN_attributes)] := {
    attribute_type := USERNAME,
    attribute_length := lengthof(pl_username),
    attribute_value := {
      username := pl_username 
    }
  }  
} //f_STUN_Append_Username

function f_STUN_Append_Message_Integrity(
  inout PDU_STUN pl_pdu,
  in OCT20 pl_message_integrity := '0000000000000000000000000000000000000000'O //dummy value will replace with the correct one while encode
){
  pl_pdu.STUN_attributes[sizeof(pl_pdu.STUN_attributes)] := {
    attribute_type := MESSAGE_INTEGRITY,
    attribute_length := 20,
    attribute_value := {
      message_integrity := pl_message_integrity 
    }
  }  
} //f_STUN_Append_Message_Integrity

function f_STUN_Append_Error_Code(
  inout PDU_STUN pl_pdu,
  in STUN_Error_Code pl_error_code
){
  pl_pdu.STUN_attributes[sizeof(pl_pdu.STUN_attributes)] := {
    attribute_type := ERROR_CODE,
    attribute_length := 4+lengthof(pl_error_code.reason_phrase),
    attribute_value := {
      error_code := pl_error_code 
    }
  }  
} //f_STUN_Append_Error_Code

function f_STUN_Append_Unknown_Attributes(
  inout PDU_STUN pl_pdu,
  in STUN_Unknown_Attributes pl_unknown_attributes
){
  pl_pdu.STUN_attributes[sizeof(pl_pdu.STUN_attributes)] := {
    attribute_type := UNKNOWN_ATTRIBUTES,
    attribute_length := 2*sizeof(pl_unknown_attributes),
    attribute_value := {
      unknown_attributes := pl_unknown_attributes 
    }
  }  
} //f_STUN_Append_Unknown_Attributes

function f_STUN_Append_Realm(
  inout PDU_STUN pl_pdu,
  in charstring pl_realm
){
  pl_pdu.STUN_attributes[sizeof(pl_pdu.STUN_attributes)] := {
    attribute_type := REALM,
    attribute_length := lengthof(pl_realm),
    attribute_value := {
      realm := pl_realm 
    }
  }  
} //f_STUN_Append_Realm

function f_STUN_Append_Nonce(
  inout PDU_STUN pl_pdu,
  in octetstring pl_nonce
){
  pl_pdu.STUN_attributes[sizeof(pl_pdu.STUN_attributes)] := {
    attribute_type := NONCE,
    attribute_length := lengthof(pl_nonce),
    attribute_value := {
      nonce := pl_nonce
    }
  }  
} //f_STUN_Append_Nonce

function f_STUN_Append_Xor_Mapped_Address(
  inout PDU_STUN pl_pdu,
  in STUN_Address pl_address
){
  pl_pdu.STUN_attributes[sizeof(pl_pdu.STUN_attributes)] := {
    attribute_type := XOR_MAPPED_ADDRESS,
    attribute_length := f_get_Address_Length(pl_address),
    attribute_value := {
      xor_mapped_address := pl_address 
    }
  }  
} //f_STUN_Append_Xor_Mapped_Address

function f_STUN_Append_Software (
  inout PDU_STUN pl_pdu,
  in charstring pl_software
) {
  pl_pdu.STUN_attributes[sizeof(pl_pdu.STUN_attributes)] := {
    attribute_type := SOFTWARE,
    attribute_length := lengthof(pl_software),
    attribute_value := {
      software := pl_software
    }
  }
} //f_STUN_Append_Software

function f_STUN_Append_Alternate_Server (
  inout PDU_STUN pl_pdu,
  in STUN_Address pl_address
) {
  pl_pdu.STUN_attributes[sizeof(pl_pdu.STUN_attributes)] := {
    attribute_type := ALTERNATE_SERVER,
    attribute_length := f_get_Address_Length(pl_address),
    attribute_value := {
      alternate_server := pl_address 
    }
  }
} //f_STUN_Append_Alternate_Server
  
function f_STUN_Append_Fingerprint (
  inout PDU_STUN pl_pdu,
  in OCT4 pl_fingerprint := '00000000'O //dummy value will replace with the correct one while encode
) {
  pl_pdu.STUN_attributes[sizeof(pl_pdu.STUN_attributes)] := {
    attribute_type := FINGERPRINT,
    attribute_length := 4,
    attribute_value := {
      fingerprint := pl_fingerprint 
    }
  }
} //f_STUN_Append_Fingerprint

function f_STUN_Append_Priority (
  inout PDU_STUN pl_pdu,
  in LIN4_BO_LAST pl_priority := 0 
) {
  pl_pdu.STUN_attributes[sizeof(pl_pdu.STUN_attributes)] := {
    attribute_type := PRIORITY,
    attribute_length := 4,
    attribute_value := {
      priority := pl_priority 
    }
  }
} //f_STUN_Append_Priority

function f_STUN_Append_Use_Candidate (
  inout PDU_STUN pl_pdu 
) {
  pl_pdu.STUN_attributes[sizeof(pl_pdu.STUN_attributes)] := {
    attribute_type := USE_CANDIDATE,
    attribute_length := 0,
    attribute_value := omit
  }
} //f_STUN_Append_Use_Candidate

function f_STUN_Append_Ice_Controlled (
  inout PDU_STUN pl_pdu,
  in OCT8 pl_ice_controlled := '0000000000000000'O 
) {
  pl_pdu.STUN_attributes[sizeof(pl_pdu.STUN_attributes)] := {
    attribute_type := ICE_CONTROLLED,
    attribute_length := 8,
    attribute_value := {
      ice_controlled := pl_ice_controlled 
    }
  }
} //f_STUN_Append_Ice_Controlled

function f_STUN_Append_Ice_Controlling (
  inout PDU_STUN pl_pdu,
  in OCT8 pl_ice_controlling := '0000000000000000'O 
) {
  pl_pdu.STUN_attributes[sizeof(pl_pdu.STUN_attributes)] := {
    attribute_type := ICE_CONTROLLING,
    attribute_length := 8,
    attribute_value := {
      ice_controlling := pl_ice_controlling 
    }
  }
} //f_STUN_Append_Ice_Controlling

private function f_get_Address_Length(in STUN_Address pl_address) return integer{
  if(pl_address.family == IP6){
    return 20;
  }
  
  return 8;
}

  
} //end of module
with {
extension "version R2B"
}

